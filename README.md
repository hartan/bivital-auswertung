# Programme und Skripte zur Arbeit mit dem BiVital System

Das BiVital ist ein WBS (Wireless Body Sensor), der an der Uni Bielefeld
seit 2006 entwickelt wird.
Im Rahmen des Moduls "*BioMechatronisches Praktikum (Uni)*" wird im
Sommersemester 2020 mit Revision 5.1 des BiVital gearbeitet, die im Rahmen der
Dissertation vom Timm Hörmann entstanden ist.

Im Rahmen des Praktikums sollen Anwendungsfälle für das BiVital System
erarbeitet und die Auswertung der Messdaten erprobt werden.
Diese Aufgaben können auch im Hinblick auf den Vergleich mit anderen,
kommerziellen System durchgeführt werden.



## Datenaufnahme mit dem BiVital

Das BiVital verfügt über einen nRF51822 Mikrocontroller von Nordic
Semiconductors, der über Bluetooth Low Energy (BLE) und ANT kommuniziert.
Im Moment wird lediglich die Datenübertragung per BLE verwendet.

Daten können folgendermaßen aufgenommen werden:

- Über ein [Python Skript][100] am Rechner
- Über die [PhyPhox App][1] für Android und iOS  
    Siehe hierzu auch den Unterordner [`phyphox`][102]



## Auswertung der aufgenommenen Messdaten

Zur Auswertung der Messdaten stehen im Unterordner [`matlab`][101] LiveSkripte zur
Verfügung, mit denen die Logdateien visualisiert werden können.
Die Skripte dienen im Moment in erster Linie der Visualisierung der Daten.

Die Bedienung der Skripte kann den Skripten selbst entnommen werden, da die
LiveSkripte selbst kommtiert sind.

Eine weiterführende Verarbeitung der Daten inklusive einer Extraktion
abstrakterer Daten wie z.B. Bewegungsabläufe oder Energieverbräuche ist
für den weiteren Verlauf des Projektes allerdings vorgesehen.





[1]: https://phyphox.org/download/ "PhyPhox App Download"
[100]: /ble_bivital_grabber.py "Python Skript"
[101]: /matlab "MATLAB Skripte"
[102]: /phyphox "PhyPhox Konfigurationsdatei"
