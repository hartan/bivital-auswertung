#!/usr/bin/env python3
"""Log sensor data from the BiVital module.

Establishes a BLE connection to the BiVital module and subscribes to the
GATT services for:

    - heart rate
    - ambient temperature
    - acceleration (x, y, z)
    - ecg (What does it mean??)

The data is logged into the logs/ subfolder.
"""

import asyncio
import logging
from bleak import BleakClient
from bleak import _logger as logger
import time
import os
import signal

from interpreters import Interpreter
import const as BLECNST

interpreter = Interpreter()

# Files to store data
log_path = "./logs/"
archive_log_path = "./old_logs/"
log_names = ["motion.log", "heart_rate.log", "environment.log"]
logfiles = []

# Create log directories
if os.path.isdir(log_path) is False:
    os.makedirs(log_path)

if os.path.isdir(archive_log_path) is False:
    os.makedirs(archive_log_path)

for log in log_names:
    if os.path.isfile(log_path + log) is True:
        print("Found old log {}. Moving to archive...".format(log))
        # Move existing logs to the archive
        os.rename(log_path + log,
                  archive_log_path + time.strftime("%Y%m%d-%H%M%S")
                  + '_' + log)
    # logfiles.append(open(log_path + log))

f_motion_log = open("logs/motion.log", "a+")
f_heart_rate_log = open("logs/heart_rate.log", "a+")
f_environment_log = open("logs/environment.log", "a+")
logfiles = [f_motion_log, f_heart_rate_log, f_environment_log]

closing = 0

# check if files empty and populate first line if they are
if os.stat("logs/motion.log").st_size == 0:
    f_motion_log.write("Timestamp, acc_x, acc_y, acc_z, ecg;\n")

if os.stat("logs/heart_rate.log").st_size == 0:
    f_heart_rate_log.write("Timestamp, heartrate;\n")

if os.stat("logs/environment.log").st_size == 0:
    f_environment_log.write("Timestamp, temperature;\n")


def keyboardInterruptHandler(signal, frame):
    """
    Handle an incoming SIGINT signal.

    Parameters
    ----------
    signal
        The signal to react to.
    frame
        The frame in which the signal was caught.

    Returns
    -------
    None.

    """
    global closing
    print("KeyboardInterrupt (ID: {}) has been caught."
          "Cleaning up...".format(signal))
    closing = 1


signal.signal(signal.SIGINT, keyboardInterruptHandler)


def motion_raw_notification_handler(sender, data):
    """
    React to a BLE notification for motion sensor data.

    Triggered in reaction to the publication of new motion data from the
    BiVital. Currently includes:
        - Acceleration in x, y, z axis
        - ECG data (Meaning??)

    Parameters
    ----------
    sender : bleak.BleakClient
        BiVital sensor note that published new data.
    data : raw data frame
        Raw data frame of the accelerometer sensor reading.

    Returns
    -------
    None.

    """
    # get actual time for logging
    act_time = time.time() * 1000
    act_time = int(act_time)

    recv_data = interpreter.interpret_mot_raw(data)

    f_motion_log.write(str(act_time) + ", "
                       + str(recv_data["acc_x"]) + ", "
                       + str(recv_data["acc_y"]) + ", "
                       + str(recv_data["acc_z"]) + ", "
                       + str(recv_data["aux"]) + ";\n")
    f_motion_log.flush()
    print("Received motion data")


def heart_rate_notification_handler(sender, data):
    """
    React to a BLE notification for heart rate sensor data.

    Writes heart rate in beats per minute to log files.

    Parameters
    ----------
    sender : bleak.BleakClient
        BiVital sensor note that published new data.
    data : raw data frame
        Raw data frame of the heart rate sensor reading.

    Returns
    -------
    None.

    """
    act_time = time.time() * 1000
    act_time = int(act_time)
    recv_data = interpreter.interpret_hr(data)
    f_heart_rate_log.write(str(act_time) + ", " + str(recv_data["hr"]) + ";\n")
    f_heart_rate_log.flush()
    print("Received heartrate data")


def environment_notification_handler(sender, data):
    """
    React to a BLE notification for environment sensor data.

    The BiVital is capable of capturing
        - Ambient Temperature
        - Ambient air pressure
        - Air humidity

    But currently only evaluates ambient temperature.
    Writes temperature in degree centegrade to log files.

    Parameters
    ----------
    sender : bleak.BleakClient
        BiVital sensor note that published new data.
    data : raw data frame
        Raw data frame of the environment sensors reading.

    Returns
    -------
    None.

    """
    act_time = time.time() * 1000
    act_time = int(act_time)
    temp_data = interpreter.interpret_env_temp(data)
    f_environment_log.write(str(act_time) + ", "
                            + str(temp_data["temp"]) + ";\n")
    f_environment_log.flush()
    print("Received environment data")


async def run(address, loop, debug=False):
    """
    Start and execute the asynchronous action loop.

    Initiates a BLE connection and registers the notification handler
    callbacks.

    Parameters
    ----------
    address : String
        Bluetooth MAC address of the BiVital sensor node to connect to.
    loop : async event loop
        The event loop, that is executed.
    debug : bool, optional
        Whether to print debug output or not. The default is False.

    Returns
    -------
    None.

    """
    global closing
    if debug:
        import sys

        aux_log = logging.getLogger("asyncio")
        aux_log.setLevel(logging.DEBUG)
        h = logging.StreamHandler(sys.stdout)
        h.setLevel(logging.DEBUG)
        aux_log.addHandler(h)
        logger.addHandler(h)

    async with BleakClient(address, loop=loop) as client:
        x = await client.is_connected()
        logger.info("Connected: {0}".format(x))

        # Register all notification handlers to listen to their BLE UUID
        await client.start_notify(BLECNST.UUID_CHARAC_MOTION_RAW,
                                  motion_raw_notification_handler)
        await client.start_notify(BLECNST.UUID_CHARAC_HR,
                                  heart_rate_notification_handler)
        await client.start_notify(BLECNST.UUID_CHARAC_ENV_TEMP,
                                  environment_notification_handler)

        while 1:
            if closing == 1:
                print("Closing")
                await client.stop_notify(BLECNST.UUID_CHARAC_MOTION_RAW)
                await client.stop_notify(BLECNST.UUID_CHARAC_HR)
                await client.stop_notify(BLECNST.UUID_CHARAC_ENV_TEMP)
                await client.disconnect()
                break
            await asyncio.sleep(2.0, loop=loop)


if __name__ == "__main__":
    os.environ["PYTHONASYNCIODEBUG"] = str(1)
    address = (
        "D0:D0:D8:62:6C:15"  # <--- Change to your device's address here
    )
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(address, loop, False))

    # close files
    for f in logfiles:
        f.close()
