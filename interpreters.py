import struct

class Interpreter():
    """ A set of interpreters used to unpack data from a BLE event (e.g. service)
    """

    def __init__(self):
        self.res = {}


    def get_data(self):
        return self.res


    def interpret_env_temp(self, data):
        data = struct.unpack('<bB', data)
        
        byte0 = data[0]
        byte1 = data[1]

        self.res["temp"] = byte0 + (byte1 / 100)

        return self.res

    def interpret_env_pressure(self, data):
        data = struct.unpack('<BBBBB', data)

        prefix = data[3] << 24 | data[2] << 16 | data[1] << 8 | data[0]
        suffix = data[4]
        self.res["pressure"] = prefix + (suffix / 100)

        return self.res

    def interpret_env_humidity(self, data):
        data = struct.unpack('<B', data)

        self.res["humidity"] = data[0]

        return self.res

    def interpret_mot_tap(self, data):
        self.res = {}
        data = struct.unpack('<B', data)
        self.res["tap"] = data[0]
        
        return self.res

    def interpret_mot_quaternion(self, data):
        self.res = {}
        data = struct.unpack('<BBBBBB', data)

        x = data[5] << 8 | data[4]
        y = data[3] << 8 | data[2]
        z = data[1] << 8 | data[0]

        self.res["x"] = x
        self.res["y"] = y
        self.res["z"] = z
        
        return self.res

    def interpret_mot_raw(self, data):
        self.res = {}
        data = struct.unpack('<hhhhhhhhhh', data)

        self.res["acc_x"] = data[0]
        self.res["acc_y"] = data[1]
        self.res["acc_z"] = data[2]

        self.res["gyro_x"] = data[3]
        self.res["gyro_y"] = data[4]
        self.res["gyro_z"] = data[5]

        self.res["mag_x"] = data[6]
        self.res["mag_y"] = data[7]
        self.res["mag_z"] = data[8]
        self.res["aux"] = data[9]
        
        return self.res


    def interpret_hr(self, data):
        """
        data is a list of integers corresponding to readings from the BLE HR monitor
        """

        byte0 = data[0]
        self.res = {}
        self.res["hrv_uint8"] = (byte0 & 1) == 0
        sensor_contact = (byte0 >> 1) & 3
        if sensor_contact == 2:
            self.res["sensor_contact"] = "No contact detected"
        elif sensor_contact == 3:
            self.res["sensor_contact"] = "Contact detected"
        else:
            self.res["sensor_contact"] = "Sensor contact not supported"
        self.res["ee_status"] = ((byte0 >> 3) & 1) == 1
        self.res["rr_interval"] = ((byte0 >> 4) & 1) == 1

        if self.res["hrv_uint8"]:
            self.res["hr"] = data[1]
            i = 2
        else:
            self.res["hr"] = (data[2] << 8) | data[1]
            i = 3

        if self.res["ee_status"]:
            self.res["ee"] = (data[i + 1] << 8) | data[i]
            i += 2

        self.res["rr"] = []
        if self.res["rr_interval"]:
            while i < len(data):
                # Note: Need to divide the value by 1024 to get in seconds
                self.res["rr"].append((data[i + 1] << 8) | data[i])
                i += 2

        return self.res

