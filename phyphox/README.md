# Erfassung der BiVital Sensordaten mit [PhyPhox][1]

Diese Datei kann in die [PhyPhox App][1] importiert werden, um die Sensordaten
des BiVital über ein bequemes UI auszulesen.


## Vorbereitung 

**Zum aktuellen Zeitpunkt ist es nicht möglich, die Datei direkt zu importieren**,
**da PhyPhox das BiVital dann nicht erkennt.**

Öffnet die Datei `BiVital.phyphox` mit einem Text-Editor und ersetzt alle
Vorkommen von

`BI-Vital D862`

mit dem Bluetooth-Namen eures BiVital:

`BI-Vital <Kennung>`

Aktuell müsstet ihr die Zeichenfolge 4 mal in der Datei finden.

Dieser Schritt ist notwendig, da zum aktuellen Zeitpunkt keine beliebigen
Geräte ausgewählt werden können.
Ob das in Zukunft möglich sein wird, ist noch unklar.
Wenn ihr den richtigen Gerätenamen nicht eintragt, könnt ihr das Experiment
(die Messung) nicht starten.



## Installation

> Zur Verwendung dieser Datei muss PhyPhox installiert sein!

Am einfachsten wird die Datei auf dem Handy runtergeladen und durch "antippen"
in die PhyPhox App importiert.

In der PhyPhox App kann das Experiment der *Sammlung* hinzugefügt werden, dann
findet ihr den aktuellen Stand des Projektes in Zukunft in der App.
Andernfalls muss das Experiment bei jedem Start neu importiert werden.

Wenn alles funktioniert hat, und sich euer BiVital im Messmodus befindet und
in Reichweite des Handy ist, könnt ihr die Messungen starten.



## Verwendung von PhyPhox zur Messdatenerfassung

So lange das Experiment läuft (beachte die Hinweise am Ende des Dokuments)
werden kontinuierlich Messdaten aufgezeichnet und grafisch dargestellt.
Es werden immer alle Daten geholt und aktualisiert, auch die Daten aus
Reitern und Grafen, die grade nicht aktiv dargestellt werden.

Wenn die Messung beendet werden soll, wir das Experiment zunächst gestoppt.
Anschließend können die Messdaten über das Experimentenmenü exportiert
werden.

Die Daten werden dann in eine zip Datei im CSV Format exportiert.



## Aktualisierung des Experiments

Um das Experiment auf eine neue Version zu aktualisieren, muss das Experiment
zuerst *aus PhyPhox gelöscht* werden. Tippt dazu auf das "3-Punkte-Menü" neben
dem BiVital Experiment auf der Übersichtsseite und wählt "Löschen" aus.

Schließt dann die PhyPhox App einmal vollständig (Task beenden / aus dem
Arbeitsspeicher entfernen) und geht vor, wie unter [Installation](#installation) 
beschrieben.



## Wichtige Anmerkungen

Aufgrund von [Limitationen in der PhyPhox App][2] ist es im Moment nicht
möglich, Messungen im Hintergrund durchzuführen.
Das bedeutet konkret, dass PhyPhox im Vordergrund laufen muss und der Bildschirm
des Handy **NICHT** ausgeschaltet sein darf.

Andernfalls bricht die Messung ab.




[1]: https://phyphox.org "PhyPhox Webseite"
[2]: https://phyphox.org/forums/showthread.php?tid=61 "PhyPhox im Hintergrund laufen lassen"
